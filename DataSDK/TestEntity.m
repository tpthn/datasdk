//
//  TestEntity.m
//  DataSDK
//
//  Created by PC Nguyen on 1/23/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "TestEntity.h"


@implementation TestEntity

@dynamic testNumber;
@dynamic testString;
@dynamic testBoolean;
@dynamic testDate;

@end
