//
//  TestEntity.h
//  DataSDK
//
//  Created by PC Nguyen on 1/23/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TestEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * testNumber;
@property (nonatomic, retain) NSString * testString;
@property (nonatomic, retain) NSNumber * testBoolean;
@property (nonatomic, retain) NSDate * testDate;

@end
