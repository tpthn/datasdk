//
//  DSMOCMonitor.h
//  DataSDK
//
//  Created by PC Nguyen on 1/22/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DSMOCInteraction.h"

typedef enum {
    MOCMonitorStatusDisable = 0,
    MOCMonitorStatusEnable
} DSMOCMonitorStatus;

extern NSString *const DSMOCMonitorSectionKey;
extern NSString *const DSMOCMonitorPredicateKey;
extern NSString *const DSMOCMonitorFilterPredicateKey;
extern NSString *const DSMOCMonitorFetchedResultsKey;

@class DSMOCMonitor;

@protocol DSMOCMonitorDelegate <NSObject>

@optional
- (void)MOCMonitor:(DSMOCMonitor *)MOCMonitor willUpdateEntity:(NSEntityDescription *)entity;
- (void)MOCMonitor:(DSMOCMonitor *)MOCMonitor didUpdateEntity:(NSEntityDescription *)entity;

- (void)MOCMonitor:(DSMOCMonitor *)MOCMonitor
			entity:(NSEntityDescription *)entity
   didChangeObject:(id)anObject
		   itemRow:(NSInteger)row
     forChangeType:(NSFetchedResultsChangeType)type
			newRow:(NSInteger)row;

- (void)MOCMonitor:(DSMOCMonitor *)MOCMonitor
			entity:(NSEntityDescription *)entity
   didChangeObject:(id)anObject
		 indexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
	  newIndexPath:(NSIndexPath *)newIndexPath;

@end

@interface DSMOCMonitor : DSMOCInteraction

@property (nonatomic, strong) NSMutableSet *lockedIndexPath;
@property (nonatomic, weak) id<DSMOCMonitorDelegate>delegate;

#pragma mark - Adding Monitoring

- (void)monitorEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate;

- (void)monitorEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch;

- (void)monitorWithFetchRequest:(NSFetchRequest *)fetchRequest;

- (void)monitorFetchRequest:(NSFetchRequest *)fetchRequest fetchImmediately:(BOOL)shouldFetch;

#pragma mark - Refresh

- (void)refreshEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch;

- (void)refreshEntity:(NSEntityDescription *)entity filterPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch;

- (void)removeFilterPredicateOnEntity:(NSEntityDescription *)entity fetchImmediately:(BOOL)shouldFetch;

- (void)fetchMonitoredEntity:(NSEntityDescription *)entity;

- (void)fetchAllMonitoredEntity;

#pragma mark - Locking

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status;

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status
				  forEntity:(NSEntityDescription *)entity;

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status
				  forEntity:(NSEntityDescription *)entity
				  itemIndex:(NSInteger)itemIndex;

- (void)enableMonitoringForAllIndexPath;

#pragma mark - Data Retrieving

- (NSManagedObject *)objectForEntity:(NSEntityDescription *)entity atItemIndex:(NSInteger)index;

- (NSInteger)fetchedCountForEntity:(NSEntityDescription *)entity;

- (NSInteger)totalFetchCount;

#pragma mark - Mapping

- (NSEntityDescription *)entityInSection:(NSInteger)section;

#pragma mark - Fetched Info

- (NSDictionary *)fetchedInfoForEntity:(NSEntityDescription *)entity;

- (NSInteger)sectionIndexForEntity:(NSEntityDescription *)entity;

- (NSFetchedResultsController *)fetchedResultsControllerForEntity:(NSEntityDescription *)entity;

@end
