//
//  DSMOCMonitor.m
//  DataSDK
//
//  Created by PC Nguyen on 1/22/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "DSMOCMonitor.h"
#import "DSMOCInteraction_Private.h"

NSString *const DSMOCMonitorSectionKey                  = @"DSMOCMonitorSectionKey";
NSString *const DSMOCMonitorPredicateKey                = @"DSMOCMonitorPredicateKey";
NSString *const DSMOCMonitorFilterPredicateKey          = @"DSMOCMonitorFilterPredicateKey";
NSString *const DSMOCMonitorFetchedResultsKey           = @"DSMOCMonitorFetchedResultsKey";

@interface DSMOCMonitor ()

@property (nonatomic, assign) BOOL shouldMonitor;
@property (nonatomic, assign) NSInteger currentSection;
@property (nonatomic, strong) NSMutableDictionary *fetchedControllers;

@end

@implementation DSMOCMonitor

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context
{
	if (self = [super initWithManagedObjectContext:context]) {
		_currentSection = 0;
	}
	
	return self;
}

#pragma mark - Adding Monitoring

- (void)monitorEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entity;
    fetchRequest.predicate = predicate;
    
    [self monitorWithFetchRequest:fetchRequest];
}

- (void)monitorEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch
{
	[self monitorEntity:entity withPredicate:predicate];
	
	if (shouldFetch) {
		[self fetchMonitoredEntity:entity];
	}
}

- (void)monitorWithFetchRequest:(NSFetchRequest *)fetchRequest
{
	NSString *cacheName = NSStringFromClass([fetchRequest.entity class]);
	
    NSFetchedResultsController *fetchedResultController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.currentMOC
                                          sectionNameKeyPath:nil
                                                   cacheName:cacheName];
    
    NSMutableDictionary *fetchedInfo = [[NSMutableDictionary alloc] init];
    [fetchedInfo setValue:fetchedResultController forKey:DSMOCMonitorFetchedResultsKey];
    [fetchedInfo setValue:fetchRequest.predicate forKey:DSMOCMonitorPredicateKey];
    [fetchedInfo setValue:@(self.currentSection) forKey:DSMOCMonitorSectionKey];
    [fetchedInfo setValue:nil forKey:DSMOCMonitorFilterPredicateKey];
    
    [self.fetchedControllers setObject:fetchedInfo forKey:fetchRequest.entity];
	
	self.currentSection++;
}

- (void)monitorFetchRequest:(NSFetchRequest *)fetchRequest fetchImmediately:(BOOL)shouldFetch
{
	[self monitorWithFetchRequest:fetchRequest];
	
	if (shouldFetch) {
		[self fetchMonitoredEntity:fetchRequest.entity];
	}
}

#pragma mark - Refresh

- (void)refreshEntity:(NSEntityDescription *)entity withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch
{
	[self updateFetchedInfoForEntity:entity setValue:predicate forKey:DSMOCMonitorPredicateKey];
	[self updateFetchedResultsControllerForEntity:entity];
	
	if (shouldFetch) {
		[self fetchMonitoredEntity:entity];
	}
}

- (void)refreshEntity:(NSEntityDescription *)entity filterPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch
{
	[self updateFetchedInfoForEntity:entity setValue:predicate forKey:DSMOCMonitorFilterPredicateKey];
	[self updateFetchedResultsControllerForEntity:entity];
	
	if (shouldFetch) {
		[self fetchMonitoredEntity:entity];
	}
}

- (void)removeFilterPredicateOnEntity:(NSEntityDescription *)entity fetchImmediately:(BOOL)shouldFetch
{
	[self refreshEntity:entity filterPredicate:nil fetchImmediately:shouldFetch];
}

- (void)fetchMonitoredEntity:(NSEntityDescription *)entity
{
	NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForEntity:entity];
	[self performFetch:fetchedResultsController];
}

- (void)fetchAllMonitoredEntity
{
	[self.fetchedControllers enumerateKeysAndObjectsUsingBlock:^(NSEntityDescription *entity, NSDictionary *fetchedInfo, BOOL *stop) {
		[self performFetch:[fetchedInfo valueForKey:DSMOCMonitorFetchedResultsKey]];
	}];
}

#pragma mark - Locking

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status
{
	self.shouldMonitor = (status == MOCMonitorStatusEnable);
}

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status
				  forEntity:(NSEntityDescription *)entity
{
	NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForEntity:entity];
	
	for (int i = 0; i < [[fetchedResultsController fetchedObjects] count]; i++) {
		[self setMonitoringStatus:status forEntity:entity itemIndex:i];
	}
}

- (void)setMonitoringStatus:(DSMOCMonitorStatus)status forEntity:(NSEntityDescription *)entity itemIndex:(NSInteger)itemIndex
{
	NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
	NSInteger section = [[fetchedInfo valueForKey:DSMOCMonitorSectionKey] integerValue];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:itemIndex inSection:section];
	
	if (status == MOCMonitorStatusEnable) {
		[self.lockedIndexPath removeObject:indexPath];
	} else if (status == MOCMonitorStatusDisable) {
		[self.lockedIndexPath addObject:indexPath];
	}
}

- (void)enableMonitoringForAllIndexPath
{
	self.shouldMonitor = YES;
	
	[self.lockedIndexPath removeAllObjects];
}

#pragma mark - Data Retrieving

- (NSManagedObject *)objectForEntity:(NSEntityDescription *)entity atItemIndex:(NSInteger)index
{
	NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForEntity:entity];
	
	NSManagedObject *fetchedObject = nil;
	NSArray *fetchedObjects = [fetchedResultsController fetchedObjects];
	
	if (index < [fetchedObjects count]) {
		fetchedObject = [fetchedObjects objectAtIndex:index];
	}
	
	return fetchedObject;
}

- (NSInteger)fetchedCountForEntity:(NSEntityDescription *)entity
{
	NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForEntity:entity];
	
	NSInteger count = [[fetchedResultsController fetchedObjects] count];
	
	return count;
}

- (NSInteger)totalFetchCount
{
	__block NSInteger count = 0;
	
    [self.fetchedControllers enumerateKeysAndObjectsUsingBlock:^(NSEntityDescription *key, NSDictionary *fetchedInfo, BOOL *stop) {
		NSFetchedResultsController *fetchedResultsController = [fetchedInfo valueForKey:DSMOCMonitorFetchedResultsKey];
		count += [[fetchedResultsController fetchedObjects] count];
	}];
	
	return count;
}

#pragma mark - FetchedResultsController Delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	if (self.shouldMonitor) {
		if ([self.delegate respondsToSelector:@selector(MOCMonitor:willUpdateEntity:)]) {
			[self.delegate MOCMonitor:self willUpdateEntity:controller.fetchRequest.entity];
		}
	}
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
	NSInteger section = [self sectionIndexForEntity:controller.fetchRequest.entity];
	NSIndexPath *checkedIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:section];

	BOOL isLock = [self.lockedIndexPath containsObject:checkedIndexPath];
	
    if (self.shouldMonitor && !isLock) {
		NSEntityDescription *entity = controller.fetchRequest.entity;
		
		if ([self.delegate respondsToSelector:@selector(MOCMonitor:entity:didChangeObject:itemRow:forChangeType:newRow:)]) {
			[self.delegate MOCMonitor:self
							   entity:entity
					  didChangeObject:anObject
							  itemRow:indexPath.row
						forChangeType:type
							   newRow:newIndexPath.row];
		} else if ([self.delegate respondsToSelector:@selector(MOCMonitor:entity:didChangeObject:indexPath:forChangeType:newIndexPath:)]) {
			[self.delegate MOCMonitor:self
							   entity:entity
					  didChangeObject:anObject
							indexPath:indexPath
						forChangeType:type
						 newIndexPath:newIndexPath];
		}
	}
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
	//--TODO: Figure out how we want to do this
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	if (self.shouldMonitor) {
		if ([self.delegate respondsToSelector:@selector(MOCMonitor:didUpdateEntity:)]) {
			[self.delegate MOCMonitor:self didUpdateEntity:controller.fetchRequest.entity];
		}
	}
}

#pragma mark - Section Mapping

- (void)reloadFetchedResultController:(NSFetchedResultsController *)fetchedResultsController
							inSection:(NSInteger)section
{
    NSError *fetchedError = nil;
    
    [fetchedResultsController performFetch:&fetchedError];
    
    if (fetchedError) {
        //--TODO: logging
    }
}

#pragma mark - Mapping

- (NSEntityDescription *)entityInSection:(NSInteger)section
{
	__block NSEntityDescription *entityDescription = nil;
	
	[self.fetchedControllers enumerateKeysAndObjectsUsingBlock:^(NSEntityDescription *key, NSDictionary *fetchedInfo, BOOL *stop) {
		if ([[fetchedInfo valueForKey:DSMOCMonitorSectionKey] integerValue] == section) {
			entityDescription = key;
		}
	}];
	
	return entityDescription;
}

#pragma mark - Fetched Info

- (NSDictionary *)fetchedInfoForEntity:(NSEntityDescription *)entity
{
	NSDictionary *fetchedInfo = [self.fetchedControllers objectForKey:entity];
	
	return fetchedInfo;
}

- (NSInteger)sectionIndexForEntity:(NSEntityDescription *)entity
{
	NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
	NSInteger sectionIndex = [[fetchedInfo valueForKey:DSMOCMonitorSectionKey] integerValue];
	
	return sectionIndex;
}

- (NSFetchedResultsController *)fetchedResultsControllerForEntity:(NSEntityDescription *)entity
{
	NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
	NSFetchedResultsController *fetchedResultsController = [fetchedInfo valueForKey:DSMOCMonitorFetchedResultsKey];
	
	return fetchedResultsController;
}

- (NSPredicate *)predicateForEntity:(NSEntityDescription *)entity
{
	NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
	NSPredicate *predicate = [fetchedInfo valueForKey:DSMOCMonitorPredicateKey];
	
	return predicate;
}

- (NSPredicate *)filterPredicateForEntity:(NSEntityDescription *)entity
{
	NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
	NSPredicate *filterPredicate = [fetchedInfo valueForKey:DSMOCMonitorFilterPredicateKey];
	
	return filterPredicate;
}

#pragma mark - Update Helpers

- (void)updateFetchedInfoForEntity:(NSEntityDescription *)entity setValue:(id)value forKey:(NSString *)key
{
    NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
    
    NSMutableDictionary *modifiedFetchedInfo = [fetchedInfo mutableCopy];
    [modifiedFetchedInfo setValue:value forKey:key];
    
	//--defensive
	if (fetchedInfo) {
		[self.fetchedControllers setObject:modifiedFetchedInfo forKey:entity];
	}
}

- (void)updateFetchedResultsControllerForEntity:(NSEntityDescription *)entity
{
    NSDictionary *fetchedInfo = [self fetchedInfoForEntity:entity];
    
    NSFetchedResultsController *fetchedResultsController = [fetchedInfo valueForKey:DSMOCMonitorFetchedResultsKey];
    NSPredicate *predicate = [fetchedInfo valueForKey:DSMOCMonitorPredicateKey];
    NSPredicate *filterPredicate = [fetchedInfo valueForKey:DSMOCMonitorFilterPredicateKey];
    
    NSMutableArray *combinedPredicates = [NSMutableArray array];
    
    if (predicate) {
        [combinedPredicates addObject:predicate];
    }
    
    if (filterPredicate) {
        [combinedPredicates addObject:filterPredicate];
    }
    
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:combinedPredicates];
    
    fetchedResultsController.fetchRequest.predicate = compoundPredicate;
    
	[self updateFetchedInfoForEntity:entity
							setValue:fetchedResultsController
							  forKey:DSMOCMonitorFetchedResultsKey];
}

#pragma mark - Private

- (void)performFetch:(NSFetchedResultsController *)fetchedResultsController
{
	NSError *error = nil;
	[fetchedResultsController performFetch:&error];
	
	if (error) {
		//--TODO: Logging
	}
}

@end
