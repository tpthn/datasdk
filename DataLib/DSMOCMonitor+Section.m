//
//  DSMOCMonitor+Section.m
//  DataSDK
//
//  Created by PC Nguyen on 3/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "DSMOCMonitor+Section.h"

@implementation DSMOCMonitor (Section)

#pragma mark - Refresh

- (void)ds_refreshEntityInSection:(NSInteger)section withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch
{
	NSEntityDescription *entity = [self entityInSection:section];
	
	[self refreshEntity:entity withPredicate:predicate fetchImmediately:shouldFetch];
}

- (void)ds_refreshEntityInSection:(NSInteger)section filterPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch
{
    NSEntityDescription *entity = [self entityInSection:section];
	
	[self refreshEntity:entity filterPredicate:predicate fetchImmediately:shouldFetch];
}

- (void)ds_removeFilterPredicateOnEntityInSection:(NSInteger)section fetchImmediately:(BOOL)shouldFetch
{
    NSEntityDescription *entity = [self entityInSection:section];
	
	[self removeFilterPredicateOnEntity:entity fetchImmediately:shouldFetch];
}

- (void)ds_refetchEntityInSection:(NSInteger)section
{
	NSEntityDescription *entity = [self entityInSection:section];
	
	[self fetchMonitoredEntity:entity];
}

#pragma mark - Locking

- (void)ds_setMonitoringStatus:(DSMOCMonitorStatus)status forSection:(NSInteger)section
{
	NSFetchedResultsController *fetchedResultsController = [self ds_fetchedResultsControllerInSection:section];
	
	for (int i = 0; i < [[fetchedResultsController fetchedObjects] count]; i++) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:section];
		[self ds_setMonitoringStatus:status forIndexPath:indexPath];
	}
}

- (void)ds_setMonitoringStatus:(DSMOCMonitorStatus)status forIndexPath:(NSIndexPath *)indexPath
{
	[self.lockedIndexPath addObject:indexPath];
}

#pragma mark - Data Retrieving

- (NSManagedObject *)ds_fetchedObjectAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionIndex = indexPath.section;
	NSFetchedResultsController *fetchedResultsController = [self ds_fetchedResultsControllerInSection:sectionIndex];
	NSManagedObject *fetchedObject = nil;
	NSArray *fetchedObjects = [fetchedResultsController fetchedObjects];
	
	if (indexPath.row < [fetchedObjects count]) {
		fetchedObject = [fetchedObjects objectAtIndex:indexPath.row];
	}
	
	return fetchedObject;
}

- (NSInteger)ds_fetchCountForEntitySection:(NSInteger)section
{
    NSFetchedResultsController *fetchedResultsController = [self ds_fetchedResultsControllerInSection:section];
	NSInteger count = [[fetchedResultsController fetchedObjects] count];
	return count;
}

#pragma mark - Fetched Info

- (NSFetchedResultsController *)ds_fetchedResultsControllerInSection:(NSInteger)section
{
	NSEntityDescription *entity = [self entityInSection:section];
	NSFetchedResultsController *fetchedResultsController = [self fetchedResultsControllerForEntity:entity];
	
	return fetchedResultsController;
}

@end
