//
//  DSMOCInteraction_Private.h
//  DataSDK
//
//  Created by PC Nguyen on 1/23/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "DSMOCInteraction.h"

@interface DSMOCInteraction ()

@property (nonatomic, strong) NSManagedObjectContext *currentMOC;

@end
