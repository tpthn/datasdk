//
//  NSManagedObjectContext+SDK.m
//  DataSDK
//
//  Created by PC Nguyen on 1/21/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "NSManagedObjectContext+SDK.h"

@implementation NSManagedObjectContext (SDK)

#pragma mark - Child MOC Handling

- (void)ns_recursiveSaveWithCompletion:(NSManagedObjectContextSaveCompletion)saveCompletion
{
    [self ns_recursiveSaveWithCompletion:saveCompletion detectChange:YES];
}

- (void)ns_recursiveSaveWithCompletion:(NSManagedObjectContextSaveCompletion)saveCompletion
                       detectChange:(BOOL)shouldDetect
{
    BOOL shouldSave = [self hasChanges] || !shouldDetect;
    
	if (shouldSave) {
        __block BOOL childSuccess = NO;
        __block NSError *saveError = nil;
        
        [self performBlockAndWait:^{
            childSuccess = [self save:&saveError];
        }];
        
        if (childSuccess) {
            [self saveParentMOC:self.parentContext withCompletion:saveCompletion];
        } else {
            [self performCompletion:saveCompletion success:NO error:saveError];
        }
        
	} else {
        
		[self performCompletion:saveCompletion success:YES error:nil];
	}
}

#pragma mark - Private

- (void)saveParentMOC:(NSManagedObjectContext*)parentMOC
       withCompletion:(NSManagedObjectContextSaveCompletion)completion
{
    if (parentMOC) {
        __block BOOL parentSuccess = NO;
        __block NSError *error = nil;
        
        [parentMOC performBlockAndWait:^{
            parentSuccess = [parentMOC save:&error];
        }];
        
        if (parentSuccess) {
            [self saveParentMOC:parentMOC.parentContext withCompletion:completion];
        } else {
            [self performCompletion:completion success:NO error:error];
        }
        
    } else {
        
        [self performCompletion:completion success:YES error:nil];
    }
}

- (void)performCompletion:(NSManagedObjectContextSaveCompletion)completion
                  success:(BOOL)success
                    error:(NSError *)error
{
    if (completion) {
        completion(success, error);
    }
}

@end
