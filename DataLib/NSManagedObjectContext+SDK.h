//
//  NSManagedObjectContext+SDK.h
//  DataSDK
//
//  Created by PC Nguyen on 1/21/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <CoreData/CoreData.h>

typedef void(^NSManagedObjectContextSaveCompletion)(BOOL success, NSError *error);




@interface NSManagedObjectContext (SDK)

#pragma mark - Child MOC Handling

/**
 * Save the child moc and propagate to all parent moc
 */
- (void)ns_recursiveSaveWithCompletion:(NSManagedObjectContextSaveCompletion)saveCompletion;

/**
 * For Unit Test Purpose
 */
- (void)ns_recursiveSaveWithCompletion:(NSManagedObjectContextSaveCompletion)saveCompletion
						  detectChange:(BOOL)shouldDetect;

@end
