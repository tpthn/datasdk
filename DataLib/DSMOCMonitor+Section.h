//
//  DSMOCMonitor+Section.h
//  DataSDK
//
//  Created by PC Nguyen on 3/25/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import "DSMOCMonitor.h"

@interface DSMOCMonitor (Section)

#pragma mark - Refresh

- (void)ds_refreshEntityInSection:(NSInteger)section withPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch;

- (void)ds_refreshEntityInSection:(NSInteger)section filterPredicate:(NSPredicate *)predicate fetchImmediately:(BOOL)shouldFetch;

- (void)ds_removeFilterPredicateOnEntityInSection:(NSInteger)section fetchImmediately:(BOOL)shouldFetch;

- (void)ds_refetchEntityInSection:(NSInteger)section;

#pragma mark - Locking

- (void)ds_setMonitoringStatus:(DSMOCMonitorStatus)status forSection:(NSInteger)section;

- (void)ds_setMonitoringStatus:(DSMOCMonitorStatus)status forIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Data Retrieving

- (NSManagedObject *)ds_fetchedObjectAtIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)ds_fetchCountForEntitySection:(NSInteger)section;

#pragma mark - Fetched Info

- (NSFetchedResultsController *)ds_fetchedResultsControllerInSection:(NSInteger)section;

@end
