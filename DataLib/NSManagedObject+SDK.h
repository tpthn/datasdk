//
//  NSManagedObject+SDK.h
//  DataSDK
//
//  Created by PC Nguyen on 1/21/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (SDK)

/***
 * Retrieving an identical object in different context
 * Return nil if nothing found
 */
- (id)ns_retrievedFromContext:(NSManagedObjectContext *)context;

#pragma mark - Fetching Helpers

/***
 * create the entity description for fetch request on context
 */
+ (NSEntityDescription *)ns_entityDescriptionInContext:(NSManagedObjectContext *)context;

#pragma mark - Insert Helpers

/***
 * insert a new entity of this type in context
 */
+ (instancetype)ns_insertIntoContext:(NSManagedObjectContext *)context;

/***
 * setting value of the entity from dictionary
 * using the date formatter for date type
 */
- (void)ns_setValueWithDictionary:(NSDictionary *)keyedValues
					dateFormatter:(NSDateFormatter *)dateFormatter;

#pragma mark - Delete Helpers

- (void)ns_deleteFromContext:(NSManagedObjectContext *)context;
- (void)ns_deleteFromCurrentContext;

@end
