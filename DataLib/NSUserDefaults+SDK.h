//
//  NSUserDefaults+SDK.h
//  DataSDK
//
//  Created by PC Nguyen on 5/13/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (SDK)

+ (void)ns_saveValue:(id)object forKey:(NSString *)key;

+ (id)ns_loadValueForKey:(NSString *)key;

@end
