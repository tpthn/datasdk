//
//  DSMOCManagerTests.m
//  DataSDK
//
//  Created by PC Nguyen on 1/21/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "DSMOCManager_Private.h"

@interface DSMOCManagerTests : XCTestCase

@property (nonatomic, assign) __block BOOL threadDone;

@end

@implementation DSMOCManagerTests

- (void)setUp
{
    [super setUp];
    
    NSURL *storeURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                              inDomains:NSUserDomainMask] lastObject];;
    [[DSMOCManager sharedManager] configurePersistentStoreDirectory:storeURL];
    [[DSMOCManager sharedManager] configureModelResource:@"DataSDK" withExtension:@"momd"];
    [[DSMOCManager sharedManager] configureSqliteFileName:@"DataSDK.sqlite"];
    [[DSMOCManager sharedManager] configurePersistentStoreType:MOCManagerStoreTypeSQL];
    
    self.threadDone = NO;
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark - MOC Creation

- (void)testCreateStoreCoordinator
{
    [[DSMOCManager sharedManager] configurePersistentStoreType:MOCManagerStoreTypeSQL];
    NSPersistentStoreCoordinator *coordinator = [[DSMOCManager sharedManager] persistentStoreCoordinator];
    XCTAssertNotNil(coordinator, @"Failed to create coordinator");
}

- (void)testSQLLiteStoreExist
{
    NSURL *directoryURL = [DSMOCManager sharedManager].persistentStoreDirectoryURL;
    NSString *sqlFileName = [DSMOCManager sharedManager].sqliteFileName;
    
    NSURL *storeURL = [directoryURL URLByAppendingPathComponent:sqlFileName];
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]];
    XCTAssertTrue(fileExist, @"sqlite file not on disk");
}

- (void)testMainMOCExist
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    XCTAssertNotNil(mainMOC, @"Failed to access main MOC");
}

- (void)testSharedUIMOCExist
{
    NSManagedObjectContext *sharedUIMOC = [[DSMOCManager sharedManager] sharedUIMOC];
    XCTAssertNotNil(sharedUIMOC, @"Failed to access shared UIMOC");
}

- (void)testBackroundMOCExist
{
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    XCTAssertNotNil(backgroundMOC, @"Failed to create background MOC");
    
    [[DSMOCManager sharedManager] removeBackgroundMOCWithProcessID:testProcess];
}

- (void)testBackgroundMOCAccess
{
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    NSManagedObjectContext *sameBackgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    XCTAssertEqualObjects(backgroundMOC, sameBackgroundMOC, @"Failed to access same background MOC");
}

- (void)testBackgroundMOCDestroy
{
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    
    [[DSMOCManager sharedManager] removeBackgroundMOCWithProcessID:testProcess];
    
    NSManagedObjectContext *differentBackgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    
    XCTAssertNotEqualObjects(backgroundMOC, differentBackgroundMOC, @"Failed to create new background MOC after destroy old one");
}

- (void)testBackroundMOCsDestroy
{
    NSString *firstProcess = @"firstProcess";
    NSString *secondProcess = @"secondProcess";
    
    NSManagedObjectContext *firstMOCBefore = [[DSMOCManager sharedManager] backgroundMOCForProcessID:firstProcess];
    NSManagedObjectContext *secondMOCBefore = [[DSMOCManager sharedManager] backgroundMOCForProcessID:secondProcess];
    
    [[DSMOCManager sharedManager] removeAllBackgroundMOCs];
    
    NSManagedObjectContext *firstMOCAfter = [[DSMOCManager sharedManager] backgroundMOCForProcessID:firstProcess];
    NSManagedObjectContext *secondMOCAfter = [[DSMOCManager sharedManager] backgroundMOCForProcessID:secondProcess];
    
    XCTAssertNotEqualObjects(firstMOCBefore, firstMOCAfter, @"Failed to mass destroy background MOC");
    XCTAssertNotEqualObjects(secondMOCBefore, secondMOCAfter, @"Failed to mass destroy background MOC");
    
    [[DSMOCManager sharedManager] removeAllBackgroundMOCs];
}

- (void)testSeparateMOC
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    NSManagedObjectContext *shareUIMOC = [[DSMOCManager sharedManager] sharedUIMOC];
    XCTAssertNotEqualObjects(mainMOC, shareUIMOC, @"shared UI MOC can't be main MOC");
    
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    XCTAssertNotEqualObjects(mainMOC, backgroundMOC, @"background MOC can't be main MOC");
    XCTAssertNotEqualObjects(shareUIMOC, backgroundMOC, @"background MOC can't be shared UI MOC");
    
    NSString *secondTestProcess = @"secondTestProcess";
    NSManagedObjectContext *secondBackgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:secondTestProcess];
    XCTAssertNotEqualObjects(backgroundMOC, secondBackgroundMOC, @"Failed to create separate background MOC");
    
    [[DSMOCManager sharedManager] removeAllBackgroundMOCs];
}

- (void)testMOCHierachy
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    NSManagedObjectContext *shareUIMOC = [[DSMOCManager sharedManager] sharedUIMOC];
    
    XCTAssertEqualObjects(shareUIMOC.parentContext, mainMOC, @"Expected: Main MOC is parent of sharedUIMOC");
    
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    
    XCTAssertEqualObjects(backgroundMOC.parentContext, shareUIMOC, @"Expected: sharedUIMOC is parent of background MOC");
    
    [[DSMOCManager sharedManager] removeBackgroundMOCWithProcessID:testProcess];
}

#pragma mark - Threading Test

- (void)testSyncSaveMainMOC
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    
    BOOL isMainThread = [NSThread isMainThread];
    
    [mainMOC ns_recursiveSaveWithCompletion:^(BOOL success, NSError *error) {
        
        BOOL isStillMainThread = [NSThread isMainThread];
        
        XCTAssertEqual(isMainThread, isStillMainThread, @"Failed to synchronously save main MOC");
        
        NSLog(@"SyncSaveMainMOC Executed");
        
    } detectChange:NO];
}

- (void)testAsyncSaveMainMOC
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    
    BOOL isMainThread = [NSThread isMainThread];
    
    [mainMOC performBlock:^{
        
        [mainMOC ns_recursiveSaveWithCompletion:^(BOOL success, NSError *error) {
            
            BOOL isStillMainThread = [NSThread isMainThread];
            
            XCTAssertNotEqual(isMainThread, isStillMainThread, @"Failed to Asynchornously save main MOC");
            
            NSLog(@"AsyncSaveMainMOC Executed");
            
            self.threadDone = YES;
            
        } detectChange:NO];
    }];
    
    XCTAssertTrue([self waitForCompletion:10.0f], @"Threading Timeout Without Completion");
}

- (void)testSyncSaveSharedUIMOC
{
    NSManagedObjectContext *sharedUIMOC = [[DSMOCManager sharedManager] sharedUIMOC];
    
    BOOL isMainThread = [NSThread isMainThread];
    
    [sharedUIMOC ns_recursiveSaveWithCompletion:^(BOOL success, NSError *error) {
        
        BOOL isStillMainThread = [NSThread isMainThread];
        
        XCTAssertEqual(isMainThread, isStillMainThread, @"Failed to synchornously save sharedUI MOC");
        
        NSLog(@"SyncSaveSharedUIMOC Executed");
        
    } detectChange:NO];
}

- (void)testSyncSaveBackgroundMOC
{
    NSString *testProcess = @"testProcess";
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    
    BOOL isMainThread = [NSThread isMainThread];
    
    [backgroundMOC ns_recursiveSaveWithCompletion:^(BOOL success, NSError *error) {
        
        BOOL isStillMainThread = [NSThread isMainThread];
        
        XCTAssertEqual(isMainThread, isStillMainThread, @"Failed to synchronously save background MOC");
        
        NSLog(@"SyncSaveBackgroundMOC Executed");
        
    } detectChange:NO];
    
    [[DSMOCManager sharedManager] removeBackgroundMOCWithProcessID:testProcess];
}

- (void)testAsyncSaveBackgroundMOC
{
    
    /***
     * A background MOC should only live temporary in background process
     * This is why we would NEVER Async save background MOC
     * They will go away as soon as the thread finished
     * And the changes won't save
     */
    
    NSString *testProcess = @"testProcess";
    
    BOOL isMainThread = [NSThread isMainThread];
    
    NSManagedObjectContext *backgroundMOC = [[DSMOCManager sharedManager] backgroundMOCForProcessID:testProcess];
    
    [backgroundMOC performBlock:^{
        
        [backgroundMOC ns_recursiveSaveWithCompletion:^(BOOL success, NSError *error) {
            
            BOOL isStillMainThread = [NSThread isMainThread];
            
            XCTAssertNotEqual(isMainThread, isStillMainThread, @"Failed to Asynchronously save background MOC");
            
        } detectChange:NO];
        
        [[DSMOCManager sharedManager] removeBackgroundMOCWithProcessID:testProcess];
        
        NSLog(@"AsyncSaveBackgroundMOC Executed");
        
        self.threadDone = YES;
        
    }];
    
    XCTAssertTrue([self waitForCompletion:10.0f], @"Threading Timeout Without Completion");
}

#pragma mark - Reset

- (void)testResetAllChanges
{
    //--TODO:
}

- (void)testWipeCoreData
{
    //--TODO:
}

#pragma mark - Private

- (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs
{
    NSDate *timeoutDate = [NSDate dateWithTimeIntervalSinceNow:timeoutSecs];
    
    do {
        
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:timeoutDate];
        
        if ([timeoutDate timeIntervalSinceNow] < 0.0) {
            break;
        }
        
    } while (!self.threadDone);
    
    return self.threadDone;
}

@end
