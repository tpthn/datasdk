//
//  DSMOCFetcherTest.m
//  DataSDK
//
//  Created by PC Nguyen on 1/23/14.
//  Copyright (c) 2014 PC Nguyen. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "DSMOCManager_Private.h"
#import "DSMOCFetcher.h"
#import "NSManagedObject+SDK.h"
#import "TestEntity.h"

#define DummyNumberLow                      3
#define DummyDoubleLow                      5.1
#define DummyBooleanLow                     NO
#define DummyStringLow                      @"a string"

#define DummyNumberHigh                     5
#define DummyDoubleHigh                     7.3
#define DummyBooleanHigh                    YES
#define DummyStringHigh                     @"b string"

@interface DSMOCFetcherTest : XCTestCase

@property (nonatomic, strong) DSMOCFetcher *mocFetcher;

@end

@implementation DSMOCFetcherTest

- (void)setUp
{
    [super setUp];
    
    NSURL *storeURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                              inDomains:NSUserDomainMask] lastObject];;
    [[DSMOCManager sharedManager] configurePersistentStoreDirectory:storeURL];
    [[DSMOCManager sharedManager] configureModelResource:@"DataSDK" withExtension:@"momd"];
    [[DSMOCManager sharedManager] configureSqliteFileName:@"DataSDK.sqlite"];
    [[DSMOCManager sharedManager] configurePersistentStoreType:MOCManagerStoreTypeInmemory];
    
    [self insertMockDataSet1];
}

- (void)tearDown
{
    [super tearDown];
    
    [[self mocFetcher] resetSortDescriptors];
    
    [[DSMOCManager sharedManager] wipeCoreData];
}

#pragma mark - Test Fetch

- (void)testFetchRequest
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[self fetchEntity]];
    
    NSArray *results = [[self mocFetcher] performFetchRequest:fetchRequest];
    
    XCTAssertNotEqual([results count], (unsigned int) 0, @"Failed to perform fetch request");
}

- (void)testFetchResultsWithPredicate
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"testNumber == %@", @(DummyNumberLow)];
    
    NSArray *results = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withPredicate:predicate];
    
    XCTAssertNotEqual([results count], (unsigned int) 0, @"Failed to fetch with predicate");
    
    for (TestEntity *entity in results) {
        XCTAssertEqualObjects(entity.testNumber, @(DummyNumberLow), @"Failed To Filter Result Value");
    }

}

- (void)testFetchResultsWithEqualPredicate
{
    NSArray *results = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testString" equalValue:DummyStringHigh];
    
    XCTAssertNotEqual([results count], (unsigned int) 0, @"Failed to fetch with equal predicate");
    
    for (TestEntity *entity in results) {
        XCTAssertEqualObjects(entity.testString, DummyStringHigh, @"Failed To Filter Result Value");
    }
}

- (void)testFetchTopWithPredicate
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"testBoolean == %@", @(DummyBooleanLow)];
    
    TestEntity *topEntity = (TestEntity *)[[self mocFetcher] fetchTopEntity:[self fetchEntity] withPredicate:predicate];
    
    XCTAssertNotNil(topEntity, @"failed to fetch top entity with predicate");
}

- (void)testFetchTopWithEqualPredicate
{
    TestEntity *topEntity = (TestEntity *)[[self mocFetcher] fetchTopEntity:[self fetchEntity] withAttribute:@"testString" equalValue:DummyStringHigh];
    
    XCTAssertNotNil(topEntity, @"failed to fetch top entity with equal predicate");
}

- (void)testAlternateFetch
{
    //--first round
    
    TestEntity *topEntity1 = (TestEntity *)[[self mocFetcher] fetchTopEntity:[self fetchEntity] withAttribute:@"testString" equalValue:DummyStringHigh];
    
    XCTAssertNotNil(topEntity1, @"failed to fetch top entity with equal predicate");
    
    NSArray *results1 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testString" equalValue:DummyStringHigh];
    
    XCTAssertNotEqual([results1 count], (unsigned int) 0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results1 count], (unsigned int) 1, @"Failed to adjust fetch limit");
    
    for (TestEntity *entity in results1) {
        XCTAssertEqualObjects(entity.testString, DummyStringHigh, @"Failed To Filter Result Value");
    }
    
    //--second round
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"testBoolean == %@", @(DummyBooleanLow)];
    
    TestEntity *topEntity2 = (TestEntity *)[[self mocFetcher] fetchTopEntity:[self fetchEntity] withPredicate:predicate];
    
    XCTAssertNotNil(topEntity2, @"failed to fetch top entity with predicate");
    
    NSArray *results2 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withPredicate:predicate];
    
    XCTAssertNotEqual([results2 count], (unsigned int) 0, @"Failed to fetch with predicate");
    XCTAssertNotEqual([results2 count], (unsigned int) 1, @"Failed to adjust fetch limit");
    
    for (TestEntity *entity in results2) {
        XCTAssertEqualObjects(entity.testBoolean, @(DummyBooleanLow), @"Failed To Filter Result Value");
    }

}

#pragma mark - Test Sort

- (void)testSortKey
{
    [[self mocFetcher] configureSortKey:@"testString" ascending:YES];
    NSArray *results1 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results1 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results1 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity1 = [results1 firstObject];
    XCTAssertNotNil(entity1, @"Failed to get first entity");
    XCTAssertEqualObjects(entity1.testString, DummyStringLow, @"Failed to sort by key ascending");
    
    [[self mocFetcher] configureSortKey:@"testString" ascending:NO];
    NSArray *results2 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results2 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results2 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity2 = [results2 firstObject];
    XCTAssertNotNil(entity2, @"Failed to get first entity");
    XCTAssertEqualObjects(entity2.testString, DummyStringHigh, @"Failed to sort by key descending");
}

- (void)testSingleSortDescriptor
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testString" ascending:YES];
    
    [[self mocFetcher] configureSortDescriptor:sortDescriptor];
    
    NSArray *results1 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results1 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results1 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity1 = [results1 firstObject];
    XCTAssertNotNil(entity1, @"Failed to get first entity");
    XCTAssertEqualObjects(entity1.testString, DummyStringLow, @"Failed to sort by sort descriptor ascending");
    
    [[self mocFetcher] resetSortDescriptors];
    
    sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testNumber" ascending:NO];
    [[self mocFetcher] configureSortDescriptor:sortDescriptor];
    NSArray *results2 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results2 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results2 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity2 = [results2 firstObject];
    XCTAssertNotNil(entity2, @"Failed to get first entity");
    XCTAssertEqualObjects(entity2.testString, DummyStringHigh, @"Failed to sort by sort descriptor descending");
}

- (void)testCombinedSortDescriptors
{
    NSSortDescriptor *numberSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testNumber" ascending:YES];
    NSSortDescriptor *stringSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testString" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:numberSortDescriptor,stringSortDescriptor, nil];
    
    [[self mocFetcher] configureSortDescriptors:sortDescriptors];
    
    NSArray *results1 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results1 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results1 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity1 = [results1 firstObject];
    XCTAssertNotNil(entity1, @"Failed to get first entity");
    XCTAssertEqualObjects(entity1.testString, DummyStringLow, @"Failed to sort by combined sort descriptors ascending");
    
    [[self mocFetcher] resetSortDescriptors];
    
    numberSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testNumber" ascending:YES];
    stringSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"testString" ascending:NO];
    sortDescriptors = [NSArray arrayWithObjects:numberSortDescriptor,stringSortDescriptor, nil];
    
    [[self mocFetcher] configureSortDescriptors:sortDescriptors];
    
    NSArray *results2 = [[self mocFetcher] fetchResultsEntity:[self fetchEntity] withAttribute:@"testBoolean" equalValue:@(DummyBooleanLow)];
    
    XCTAssertNotEqual([results2 count], (unsigned int)0, @"Failed to fetch with equal predicate");
    XCTAssertNotEqual([results2 count], (unsigned int)1, @"Failed to return value set");
    
    TestEntity *entity2 = [results2 firstObject];
    XCTAssertNotNil(entity2, @"Failed to get first entity");
    XCTAssertEqualObjects(entity2.testString, DummyStringHigh, @"Failed to sort by combined sort descriptor descending");
}

#pragma mark - Mock Data

- (void)insertMockDataSet1
{
    /***
     * Do not alternate test value in this set
     * Change value using the #define section
     * Create a separate set if you want to use for addition set of test
     */
    
    TestEntity *createdEntity1 = [TestEntity ns_insertIntoContext:[self fetchMOC]];
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setValue:@(DummyNumberLow) forKey:@"testNumber"];
    [attributes setValue:DummyStringLow forKey:@"testString"];
    [attributes setValue:@(DummyBooleanLow) forKey:@"testBoolean"];
    [attributes setValue:[NSDate date] forKey:@"testDate"];
    
    [createdEntity1 ns_setValueWithDictionary:attributes dateFormatter:nil];

    TestEntity *createdEntity2 = [TestEntity ns_insertIntoContext:[self fetchMOC]];
    [attributes setValue:@(DummyNumberHigh) forKey:@"testNumber"];
    [attributes setValue:DummyStringHigh forKey:@"testString"];
    [attributes setValue:@(DummyBooleanHigh) forKey:@"testBoolean"];
    [attributes setValue:[NSDate date] forKey:@"testDate"];
    
    [createdEntity2 ns_setValueWithDictionary:attributes dateFormatter:nil];

    TestEntity *createdEntity3 = [TestEntity ns_insertIntoContext:[self fetchMOC]];
    [attributes setValue:@(DummyNumberLow) forKey:@"testNumber"];
    [attributes setValue:DummyStringHigh forKey:@"testString"];
    [attributes setValue:@(DummyBooleanLow) forKey:@"testBoolean"];
    [attributes setValue:[NSDate date] forKey:@"testDate"];
    
    [createdEntity3 ns_setValueWithDictionary:attributes dateFormatter:nil];
    
    [self saveFetchMOCWithCompletion:nil];
}

#pragma mark - Private

- (NSManagedObjectContext *)fetchMOC
{
    NSManagedObjectContext *mainMOC = [[DSMOCManager sharedManager] mainMOC];
    return mainMOC;
}

- (void)saveFetchMOCWithCompletion:(NSManagedObjectContextSaveCompletion)completion
{
    [[DSMOCManager sharedManager] saveMainMOCWithCompletion:completion];
}

- (NSEntityDescription *)fetchEntity
{
    NSEntityDescription *entity = [TestEntity ns_entityDescriptionInContext:[self fetchMOC]];
    
    return entity;
}

- (DSMOCFetcher *)mocFetcher
{
    if (!_mocFetcher) {
        _mocFetcher = [[DSMOCFetcher alloc] initWithManagedObjectContext:[self fetchMOC]];
    }

    return _mocFetcher;
}

@end
