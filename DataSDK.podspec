Pod::Spec.new do |spec|
  spec.name         = 'DataSDK'
  spec.version      = '0.1.0'
  spec.license      = 'MIT' 
  spec.homepage     = 'https://bitbucket.org/tpthn/datasdk'
  spec.authors      = { 'PC Nguyen' => 'tpthn@yahoo.com' }
  spec.summary      = 'Objective C wrapper for threadsafe and convenient core data operation'
  spec.source       = { :git => 'https://tpthn@bitbucket.org/tpthn/datasdk.git',
						:branch => 'master' }

  spec.requires_arc = true
  spec.ios.deployment_target = '6.0'
  spec.source_files = 'DataLib/*.{h,m}'
  spec.frameworks   = 'CoreData'
end